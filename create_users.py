# Script to create users from old gitlab import/export json file GitLab API

import requests
import json

gitlab_url = "http://127.0.0.1:8929"
private_token = "API_TOKEN"

headers = {"Private-Token": private_token}

def create_user(email, name, username, password):
    data = {
        "email": email,
        "public_email": email,
        "name": name,
        "username": username,
        "password": password,
        "skip_confirmation": True
    }

    response = requests.post(f"{gitlab_url}/api/v4/users", headers=headers, data=data)
    
    if response.status_code == 201:
        print(f"User {username} created successfully")
    else:
        print(f"Failed to create user {username}. Error: {response.text}")

import json

# Load the JSON file
with open('project.json') as f:
    data = json.load(f)

# Iterate through the project members and print the username
for member in data['project_members']:
    username = member['user']['username']
    email = member['user']['email']
    first_name, last_name = email.split(".")[0].title(), email.split(".")[1].split("@")[0].title()
    print(username)
    print(email)
    print(first_name)
    print(last_name)

    # Create the user
    create_user(email, f"{first_name} {last_name}", username, "pasxswxord1x2x3")