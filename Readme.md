# Gitlab Poject export json to ndjson conversion
The Code was used to transfer a Gitlab project from 12.9 -> 15.11
Using one intermediate version 15.4 (last one to accept the old json import).

## Findings
### The Gitlab export format has changed (json->ndjson):
With Gitlab 15.11, the json export support was removed: https://docs.gitlab.com/ee/user/project/settings/import_export.html#compatibility 

**Solution: Import into Docker with version <15.11 and export in the new ndjson format (works with version 15.5.9)

### Retain the mapping of Issues <> User:
The following requirements must be met for this: https://docs.gitlab.com/ee/user/project/settings/import_export.html#migrating-to-gitlab-self-managed

Users must exist in the intermediate version as well as during import, and the public email must be the same.
In addition, the import must be done by an admin.

**Solution**: Create users in Docker before importing and ensure that the users also exist in the target Gitlab during import. And perform the import as an admin.

## Step by step
To keep the users they need to be created in the intermediate version:
- Download Project export from old instance
- Extract the project.json from the export tar.gz
- Start up the docker gitlab instance with docker-compose up
- Log in user root and password from docker shell -> cat /etc/gitlab/initial_root_password
- Generate private API token with root user
- Run the create_users.py
- Import the tar.gz into the Docker gitlab instance
- Export the project (now in new format)
- Import into target gitlab instance (as admin). Make sure users exist and public_email matches
